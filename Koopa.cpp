#include "Koopa.h"

Koopa::Koopa(QPoint pos,Direction direction){

    //le koopa n'est pas dans sa carapace
    inShell = false;
    isKoopa = true;

    dir = direction;

    // plusieurs images pour Koopa
    pixmap_walk[0] = QPixmap(":/images/turtle-walk-1.png");
    pixmap_walk[1] = QPixmap(":/images/turtle-walk-1.png");
    pixmap_turtle = QPixmap(":/images/turtle-turtleback-0.png");

    //On commence avec cette image
    setPixmap(pixmap_walk[0]);
    setPos(pos - QPoint(0,pixmap().height()));
}

void Koopa::moving()
{
    //Si Koopa en mouvement et pas dans sa carapace
    if(move && !inShell)
        //On alterne les images pour créer l'animation
        setPixmap(pixmap_walk[(walk_count++/walk_frame)%2]);

    //Change le sens de déplacement à gauche
    if(dir == RIGHT)
        setPixmap(pixmap().transformed(QTransform().scale(-1,1)));

}

void Koopa::hit(Item *what, Direction fromDir){

    what=nullptr;

    //Si Koopa touche un autre objet il change de direction
    if(fromDir == RIGHT || fromDir == LEFT)
        setDir(inverseDirection(dir));
}

void Koopa::hurt()
{
    // Koopa se cache dans sa coquille
    if(inShell){
        move = true;
    }
    else
    {
        //Koopa rentre dans sa carapace
        //lorsqu'il est hit par mario
        setPixmap(pixmap_turtle);
        inShell = true;
    }
}
