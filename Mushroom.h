#ifndef MUSHROOM_H
#define MUSHROOM_H
#include <QPoint>
#include "Entity.h"
#include "Item.h"

//Classes représentant un champignon
//donnant un bonus de vie à Mario et augmentant sa taille
class Mushroom : public Entity
{
    protected:

        QPoint spawned_position;

    public:

        Mushroom(QPoint position);

        virtual void moving();

        virtual void hit(Item *what, Direction fromDir);
};

#endif // MUSHROOM_H
