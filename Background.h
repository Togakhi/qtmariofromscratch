#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "Inert.h"

class Background : public Inert
{
    protected:

        std::string texture_path;

    public:

        Background(QPoint position, std::string _texture_path);

        virtual void hit(Item *what, Direction fromDir);
};

#endif // BACKGROUND_H
