#include "Block.h"
#include <QSound>


Block::Block(QPoint pos)
{
    setPixmap(QPixmap(":/images/block.png"));
    this->pos = pos;
    setPos(    this->pos);
}

void Block::hit(Item * what, Direction fromDir)
{
    what=nullptr;

    if(fromDir == DOWN)
        //Mettre le son
        QSound::play(":/sons/block-hit.wav");
}
