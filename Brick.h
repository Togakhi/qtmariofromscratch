#ifndef BRICK_H
#define BRICK_H
#include "Inert.h"

class Brick: public Inert
{
    protected:

        bool knocked;				// si le brick est touché par Mario

        bool destroyed;				// si le brick est detruit par mario

        int  count;                 // compteur pour les différents images du brick

    public:

        Brick(QPoint pos);

        virtual void moving() ;

        virtual void hit(Item *what, Direction fromDir);

};
#endif // BRICK_H
