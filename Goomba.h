#ifndef GOOMBA_H
#define GOOMBA_H

#include "Enemy.h"

class Goomba : public Enemy{

    protected:

        //Pixmaps
        QPixmap pixmap_walk[2];
        QPixmap pixmap_dead;

        //diviseur d'animation (on ne sait pas trop à quoi ça sert mais on le met quand même)
        static const int walk_frame = 10;

    public:

        Goomba(QPoint pos, Direction dir = RIGHT);

        virtual void hurt();

        virtual void hit(Item *what, Direction fromDir);

        virtual void moving();

};

#endif // GOOMBA_H
