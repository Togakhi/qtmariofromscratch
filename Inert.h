#ifndef INERT_H
#define INERT_H
#include <QGraphicsPixmapItem>

#include "Item.h"

class Inert :  public Item
{

    public:
        Inert();

        virtual void checkStatus() { /* ne fait rien */ }

        virtual void moving()   { /* ne fait rien */ }

        //Méthode qui sera implémentée par les entités inertes
        virtual void hit(Item *what, Direction fromDir) = 0;
};

#endif // INERT_H
