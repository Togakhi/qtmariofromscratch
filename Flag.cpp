#include "Flag.h"
#include "Mario.h"


Flag::Flag(QPoint position) : Inert()
{

    setPixmap(QPixmap(loadTexture(":/images/mario-flag---image.png")));

    setPos(position-QPoint(0, pixmap().height()));

    collidable = true;
}

void Flag::hit(Item *what, Direction fromDir)
{
    what = nullptr;
    fromDir = UNKNOWN;
}
