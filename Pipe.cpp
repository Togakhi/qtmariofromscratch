#include "Pipe.h"


Pipe::Pipe(QPoint position, int size) : Inert()
{
    // set plusieurs type de pipe
    if(size == 0)

        setPixmap(QPixmap(loadTexture(":/images/pipe-small.png")));
    else if(size == 1)

        setPixmap(QPixmap(loadTexture(":/images/pipe-med.png")));

    else
        setPixmap(QPixmap(loadTexture(":/images/pipe-big.png")));

    //je ne sais pas encore
    setPos(position-QPoint(0, pixmap().height()));
}

void Pipe::hit(Item *what, Direction fromDir)
{
    what = nullptr;
    fromDir = UNKNOWN;
}
