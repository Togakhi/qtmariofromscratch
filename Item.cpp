#include "Item.h"
#include "Game.h"


Item::Item() : QGraphicsPixmapItem(0)
{
    //Tous les items peuvent entrer en collision par défaut
    collidable = true;

    //Tous les items ne peuvent pas tous se faire marcher dessus par défaut
    walkable   = false;

    //met la limite des collisions à des rects (du Pixmap de l'item)
    setShapeMode(QGraphicsPixmapItem::BoundingRectShape);

    //ajout de l'item dans la scene (quand la classe Game sera implémentée)
    Game::instance()->getScene()->addItem(this);
}



//Méthode retournant la direction de collision de deux items
//Pour plus d'info : https://doc.qt.io/archives/qt-4.8/qrect.html#intersected
Direction Item::collisionDir(Item * touchingItem)
{
    //On récupère les bordures (rectangles) de l'item en cours et de l'item en paramètre
    QRectF current_rect =  sceneBoundingRect();
    QRectF item_rect =  touchingItem->sceneBoundingRect();

    // S'il n'y a pas d'intersection entre les rectangles pas de direction
    //(car pas de rectangle d'intersection)
    if(!current_rect.intersects(item_rect))
        return UNKNOWN;

    //S'il y a intersection, on récupère le rectangle d'intersection
    QRectF interRect = current_rect.intersected(item_rect);

    //si le rectangle d'intersection est vertical => collision horizontale
    if(interRect.width() < interRect.height())
    {
        //Si le coté gauche du rectangle d'intersection coincide avec
        //le coté gauche du rectangle en cours la collision vient de la gauche
        if(interRect.left() == current_rect.x())
            return LEFT;
        //Sinon elle vient de la droite
        else
            return RIGHT;
    }
    //si le rectangle d'intersection est horizontal => collision verticale
    else
    {
        //Si le côté supérieur du rectangle d'intersection coincide avec
        //le coté supérieur du rectangle en cours la collision vient d'en haut
        if(interRect.top() == current_rect.y())
            return UP;
        //Sinon elle vient d'en bas
        else
            return DOWN;
    }
}



