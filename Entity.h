#ifndef ENTITY_H
#define ENTITY_H

#include "Item.h"

class Entity : public Item {

protected:

    bool isKoopa = false; // si l'enttité est un koopa

    int move_speed;     // vitesse de déplacements de l'entité
    int move_speed_div; // vitesse de déplacements pour les unités lentes (slow)
    int jump_speed;     // vitesse de sauts
    int jump_duration;  // durée du saut


    //états de l'entité
    Direction dir;      // direction vers laquelle l'entité se déplace
    bool move;          // si l'entité est en déplacement
    bool jump;          // si l'entité est en train de sauter (mario)
    bool fall;          // si l'entité est en train de tomber
    bool untouchable;   // si l'entité peut être touché
    bool collectable;   // si l'entité peut être récolté
    bool dying;         // si l'entité est en train de mourir (mario : -1 vie)
    bool dead;          // si l'entité est morte (mario => game over)
    bool freeze;        // si l'entité ne peut plus bouger (mario après un game over par exemple)
    bool slow;          // si l'entité est ralentie (ennemis seulement)

    //compteurs
    int jump_count;
    int death_count;
    int walk_count;
    int untouchable_count;
    int freeze_count;

    //durée
    int fall_speed;
    int death_duration;
    int untouchable_duration;
    int freeze_duration;

    //ancienne position de l'entité
    QPointF prevPos;
    Item *walkable_item;

    //méthode action
    virtual void startJump();
    virtual void endJump();
    virtual void startUntouchable();
    virtual void endUntouchable();
    virtual void freezed(){ freeze = true;}

public:
    Entity();

    //Getters & Setters
    inline bool isDead() const {return dead;}
    inline bool isDying() const { return dying;}
    inline bool isUntouchable() const {return untouchable;}
    inline bool isCollectable() const { return collectable;}
    inline bool isFalling(){return fall;}
    inline virtual void setDir(Direction Dir){ dir = Dir;}
    inline virtual void setMoving(bool moving){ if((!dying)||(!dead)) move = moving;}
    virtual void hit(Item *what, Direction fromDir) = 0;

    virtual void moving() = 0;
    virtual void checkStatus();
    virtual void manageCollisions();

    virtual void die();
};

#endif // ENTITY_H
