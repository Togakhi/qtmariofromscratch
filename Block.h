#ifndef BLOCK_H
#define BLOCK_H
#include "Inert.h"

class Block : public Inert
{
    protected:

        QPoint pos;

    public:

        Block(QPoint pos);

        virtual void hit(Item *what, Direction fromDir);
};

#endif // BLOCK_H
