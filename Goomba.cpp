#include "Goomba.h"
#include <QSound>

Goomba::Goomba(QPoint pos,Direction direction){

    //Plusieurs image pour représenter Goomba lors
    //du déplacement ou de sa mort
    pixmap_walk[0] = QPixmap(":/images/goomba-0.png");
    pixmap_walk[1] = QPixmap(":/images/goomba-1.png");
    pixmap_dead = QPixmap(":/images/goomba-dead.png");

    dir = direction;

    //On commence avec cette image
    setPixmap(pixmap_walk[0]);
    setPos(pos - QPoint(0,pixmap().height()));
}

void Goomba::moving()
{
    //si Goomba meurt ou en train de mourir
    //on met l'image de sa mort
    if(dying || dead){
        setPixmap(pixmap_dead);

    //Si Goomba est en mouvement
    }else if(move){

        //On utilise les deux images alternativement
        setPixmap(pixmap_walk[(walk_count++/walk_frame)%2]);
    }

    //Change le sens de déplacement à gauche
    if(dir == LEFT)
        setPixmap(pixmap().transformed(QTransform().scale(-1,1)));
}


void Goomba::hit(Item *what, Direction fromDir){

    what=nullptr;

    //Si Goomba touche un autre objet il change de direction
    if(fromDir == RIGHT || fromDir == LEFT)
        setDir(inverseDirection(dir));
}

void Goomba::hurt()
{
    QSound::play(":/sons/stomp.wav");
    die();
}

