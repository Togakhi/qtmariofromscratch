#include "Game.h"
#include "Item.h"
#include <QDebug>
#include <QFont>
Game * Game::uniqueInstance = 0;


Game* Game::instance(){

    if(uniqueInstance == 0){
        uniqueInstance = new Game();
    }
    return uniqueInstance;
}



Game::Game(QGraphicsView * parent) : QGraphicsView(parent){

    mario = 0;
    scene = new QGraphicsScene();


    setScene(scene);

    scale(2.0,2.0);

    centerOn(0,0);
    setInteractive(false);

    //désactive le déplacement avec les barres pour scroler
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    //Connexion du moteur du jeu au slot checkStatus
    //qui va faire en sorte que tous les 10ms on va appeler la
    //méthode checkstatus
    QObject::connect(&gameTimer, SIGNAL(timeout()), this, SLOT(checkStatus()));
    gameTimer.setInterval(10);

    //taille de la scene
    scene->setSceneRect(0,0,6000,225);

    //On lance la musique
    music = new QSound(":/sons/overworld.wav");
    music->setLoops(QSound::Infinite);
    mario = 0;
    reset();

    //window size
    this->setFixedHeight(2.0*225);
    this->setFixedWidth(800);
}

void Game::checkStatus()
{
    // Ne rien faire si la partie est en cours
    if(current_state != RUNNING)
        return;

    // Si mario est moirt, la partie est finie
    if(mario->isDead()) {
        gameOver();
    }

    //met fin à la partie si mario touche le chateau
    if(mario->isGameFinished()){
        gameSuccess();
    }

    // Si mario est en train de se transformer ou de mourir toutes les autres entités sont gelées
    if(mario->isDying() || mario->isTransforming())
    {
        mario->checkStatus();
        mario->moving();
        return;
    }

    // On donne vie à toutes les autres entités de la partie
    for(auto & item : scene->items())
    {
        Item* it = dynamic_cast<Item*>(item);
        if(it)
        {
            it->checkStatus();
            it->moving();

            // On détruit toutes le entités mortes
            Entity* entity_item = dynamic_cast<Entity*>(it);
            if(entity_item && entity_item->isDead())
            {
                scene->removeItem(entity_item);
                delete entity_item;
            }
        }
    }

    //On affiche le score
    scores->setPlainText("Score: "+ QString::number(mario->getScores() ));

    //On centre la vue sur Mario
    centerOn(mario);

    // Text score postion relative par rapport mario
    if(mario->x()<190)
         scores->setPos(300,5);
    else
         scores->setPos(mario->x()+110,5);

}

//Fin de la partie
void Game::gameSuccess()
{
    current_state = SUCCESS;
    gameTimer.stop();

    // On met un fond d'écran noir
    scene->setBackgroundBrush(QBrush(Qt::green));

    //On arrête la musique du jeu et on joue la musique de fin de la partie

    QSound::play(":/sons/game-win.wav");
    music->stop();

    //A voir pour le message de succès de la partie
}


// Commencement d'une nouvelle partie
void Game::start()
{
    //Si l'état de la partie est prêt on la lance
    if(current_state == READY)
    {
        scene->clear();
        gameTimer.start();
        mario = GameManager::load("World-1-1", scene);

        if(!mario)
        {
            scene->setBackgroundBrush(QBrush(QColor(242,204,204)));
            QGraphicsTextItem* text = scene->addText("Erreur pendant le chargement de la partie");
            text->setPos(300,90);
            centerOn(text);
        }
        else
            music->play();

       //On gère le score
       scores = scene->addText("Score: "+ QString::number(mario->getScores() ));
       scores->setFont( QFont("consolas",14));
       current_state = RUNNING;
    }
}



// On reset la partie
void Game::reset()
{
    current_state = READY;
    mario = 0;
    gameTimer.stop();
    music->stop();
    scene->clear();

    //Le temps de mettre une image en background
    scene->setBackgroundBrush(QImage(":/images/wallpaper-mario-poster-mario.jpg"));


    //On affiche le message de bienvenue
    QGraphicsTextItem* text = scene->addText("Appuyez sur Entrée pour commencer");
    text->setDefaultTextColor(Qt::white);
    text->setPos(90,30);
    centerOn(text);
}



//Fin de la partie
void Game::gameOver()
{
    current_state = GAME_OVER;
    gameTimer.stop();

    // On met un fond d'écran noir
    scene->setBackgroundBrush(QBrush(Qt::black));

    //On joue la musique qui correspond à cet état
    QSound::play(":/sons/gameover.wav");
}



//On gère toutes les interractions avec l'utilisateur ici
void Game::keyPressEvent(QKeyEvent * event)
{

    // On commence une nouvelle partie en appuyant sur entrée
    if(event->key() == Qt::Key_Enter && current_state == READY)
        start();

    // On recommence la partie
    if(event->key() == Qt::Key_R || current_state == GAME_OVER)
        reset();

    // Gestion des mouvements de Mario
    if(event->key() == Qt::Key_Right || event->key() == Qt::Key_Left)
    {
        mario->setMoving(true);
        mario->setDir(event->key() == Qt::Key_Right ? Direction::RIGHT : Direction::LEFT);
    }

    // Gestion du saut de Mario
    if(event->key() == Qt::Key_Space)
        mario->toJump();

}



void Game::keyReleaseEvent(QKeyEvent *event)
{
    // Si la partie n'a pas commencé on ne fait rien
    if(current_state != RUNNING)
        return;

    // Mouvements de mario
    if(event->key() == Qt::Key_Right || event->key() == Qt::Key_Left)
        mario->setMoving(false);
}



// Arret de la musique
void Game::stopMusic()
{
    if(music)
        music->stop();
}

