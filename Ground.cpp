#include "Ground.h"
#include <QPainter>

Ground::Ground(QRect rect) : Inert()
{

    //set la position du sol
    setPos(QPoint(rect.x(), rect.y()));

    //Création du sol à partir de plusieurs image  "wall"
    QPixmap collage(loadTexture(":/images/wall.png"));

    collage = collage.scaled(rect.width(), rect.height());

    QPainter painter(&collage);

    QPixmap single_block(loadTexture(":/images/wall.png"));

    for(int y=0; y<rect.height(); y += single_block.height())
        for(int x=0; x<rect.width(); x += single_block.width())
            painter.drawPixmap(x, y, single_block);

    //On set l'image pour le sol
    setPixmap(collage);
}

void Ground::hit(Item *what, Direction fromDir)
{
    what = nullptr;
    fromDir=UNKNOWN;
}

