#include "Entity.h"
#include "Game.h"
#include "Enemy.h"


Entity::Entity() : Item()
{
    //Les vitesses des différentes actions
    move_speed = 1;
    jump_speed = 2;
    fall_speed = 2;
    move_speed_div = 2;


    //états (entity)
    if(isKoopa)
        dir = LEFT;
    else
        dir = RIGHT;
    move = true;
    jump = false;
    fall = true;
    untouchable = false;
    dying = false;
    dead = false;
    freeze = false;
    slow = false;
    collectable = false;

    //compteurs
    jump_count = 0;
    death_count = 0;
    walk_count = 0;
    untouchable_count = 0;
    freeze_count = 0;

    //limites de compteurs
    jump_duration = 30;
    death_duration = 100;
    untouchable_duration = 200;
    freeze_duration = 80;


    //Item sur lesquels on peut marcher
    walkable_item = 0;

}

void Entity::die()
{
    //on check si l'entité est bien morte
    if((!dying)&&(!dead)){
        dying = true;
        death_count = 0;
        untouchable = true;
        move = false;
    }
}


void Entity::checkStatus(){

    //status entité gelé : ne fais rien si l'entité est gelée
    if(freeze){
        freeze_count++;
        if(freeze_count > freeze_duration){
            freeze = false;
            freeze_count = 0;
        }else
            return;
    }

    //status entité en déplacement
    if(move){
        //on init l'ancienne position de l'entité
        prevPos = pos();

        //si l'entité est un ennemi (slow à true)
        if(slow)
            //on divise sa vitesse de déplacement
            move_speed = walk_count % move_speed_div == 0;

        if(dir == RIGHT)
            setX(x() + move_speed);
        else if(dir == LEFT)
            setX(x() - move_speed);
        else if(dir == UP)
            setY(y() - move_speed);
        else if(dir == DOWN)
            setY(y() + move_speed);

        //On appelle la méthode de collision
        //en cas de collision durant le mouvement
        manageCollisions();
    }

    //status entité en saut
    if(jump){
        //on init l'ancienne pos de l'entité
        prevPos = pos();

        //gere le saut
        setY(y() - jump_speed);

        //lance le compteur
        jump_count += jump_speed;

        // finis le saut quand on atteinds la durée maximale du saut
        if(jump_count > jump_duration)
            endJump();
        manageCollisions();
    }

    //si mario ne marche plus sur un item sur lequel on peut marcher
    //mario doit recommencer à tomber
    if(walkable_item && !collisionDir(walkable_item)){
        fall = true;
    }

    //status entité en train de tomber
    if(fall){
        //on init l'ancienne pos de l'entité
        prevPos = pos();

        setY(y() + fall_speed);
        manageCollisions();
    }

    //si mario tombe en bas de la scene = mort direct
    if(y() > Game::instance()->getScene()->sceneRect().height() && !dying)
        die();

    //si mario se fait blesser
    if(dying){
        death_count++;
        if(death_count > death_duration)
            dead = true;
    }
}

void Entity::manageCollisions()
{
    //si l'entitée est déjà morte ou qu'elle ne peut pas entrer en collision
    //avec d'autres entités, on ne fait rien
    if(!collidable || dead)
        return;

    //on récupére les collisions
    QList<QGraphicsItem*> colliding_items = collidingItems();

    //si l'entité avec laquelle on rentre en collisions est intouchable
    bool revertCollisions = false;

    //gére les collisions
    for(auto & col_item : colliding_items)
    {

        Item * item = dynamic_cast<Item *>(col_item);

        //si la conversion ne marche pas, on passe au prochain item
        if(!item)
            continue;

        //si l'entité ne peux pas entrer en collision, on passe au prochain item
        if(!item->isCollidable())
            continue;


        //si l'entité est intouchable temporairement, on ignore la collision
        Entity * entity_item = dynamic_cast<Entity*>(item);
        if(entity_item && (entity_item->isUntouchable() || untouchable))
            continue;

        //ignore la collision entre les ennemis et les collectable de mario
        if((dynamic_cast<Enemy *>(item) && this->isCollectable()) ||
               (entity_item && entity_item->isCollectable() && dynamic_cast<Enemy*>(this)))
            continue;

        //on récupère la direction d'où provient la direction
        Direction coll_dir = collisionDir(item);

        //si la collision à déjà était gérée
        if(!coll_dir)
            continue;


        //si on rentre en collision avec un objet sur lequel on peut marcher
        if(coll_dir == DOWN && fall && item->isWalkable())
        {
            fall=false;
            walkable_item = item;
        }

        //si on rentre en collision avec un objet lorsqu'on saute
        if(coll_dir == UP && jump)
        {
            endJump();
        }


        //si on rentre en collision directement avec l'autre item
        item->hit(this,inverseDirection(coll_dir));
        this->hit(item,coll_dir);


        //si on arrive ici, cela veut dire que l'on rentre en collision
        //avec un objet qui ne peut pas rentrer en collisions
        revertCollisions = true;


    }

    //on revient à la précédente position si revertCollisions est à true
    if(revertCollisions)
        setPos(prevPos);


}

//Méthode pour commencer un saut
void Entity::startJump()
{
    // teste si l'entité n'est pas déjà en train de sauter ou en train de tomber
    if(!fall && !jump)
    {
        //on met l'entité susceptible de se faire marcher dessus par mario à 0
        //(dans le cas où il a déjà marcher sur une entité
        walkable_item = 0;
        jump = true;
    }
}


//Méthode pour finir un saut
void Entity::endJump()
{
    if(jump)
    {
        jump = false;
        jump_count = 0;
        fall = true;
    }
}


//Méthode pour commencer la phase ou Mario est intouchable
void Entity::startUntouchable()
{
    if(!untouchable)
    {
        untouchable = true;
        untouchable_count = 0;
        //va nous permettre de modifier l'opacité de mario (le rendre transparent transparent)
        setOpacity(0.5);
    }
}


//Méthode pour finir la phase ou Mario est intouchable
void Entity::endUntouchable()
{
    if(untouchable)
    {
        untouchable = false;
        untouchable_count = 0;
        setOpacity(1.0);
    }
}
