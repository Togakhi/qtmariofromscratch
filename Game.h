#ifndef GAME_H
#define GAME_H

#include <QKeyEvent>
#include <QApplication>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QTimer>
#include <QSound>
#include "Mario.h"
#include "GameManager.h"
#include "Flag.h"

class Game : public QGraphicsView
{

    Q_OBJECT

    enum game_state{READY, RUNNING, GAME_OVER, SUCCESS};


    private :
        static Game * uniqueInstance;

        Game(QGraphicsView * parent = 0);

        game_state current_state;

        QGraphicsScene *scene;

        QTimer gameTimer;

        Mario * mario;

        QSound *music;

        QGraphicsTextItem* scores ;


    protected:

        virtual void keyPressEvent(QKeyEvent *event);

        virtual void keyReleaseEvent(QKeyEvent *e);

    public:

        static Game* instance();

        QGraphicsScene* getScene(){return scene;}

    public slots:

        void checkStatus();

        // redémarrer la partie
        void reset();

        // commencer une nouvelle partie
        void start();

        // partie finie
        void gameOver();

        //met fin à la partie
        void gameSuccess();

        // arret de la musique
        void stopMusic();


};

#endif // GAME_H
