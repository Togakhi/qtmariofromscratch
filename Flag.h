#ifndef FLAG_H
#define FLAG_H

#include "Inert.h"

class Flag : public Inert
{
    public:

        Flag(QPoint position);

        virtual void hit(Item *what, Direction fromDir);
};

#endif // FLAG_H
