#include "Brick.h"
#include "BrickDebris.h"
#include "Mario.h"
#include <QSound>

Brick::Brick(QPoint pos): Inert()
{
    setPixmap(QPixmap(":/images/brick.bmp"));

    setPos(pos);

    knocked = false;

    destroyed = false;

    count = 0;
}

//Méthode gérant les collisions avec mario
void Brick::moving()
{

    //Si la brick n'est pas visible ne rien faire
    if(this->isVisible() == 0)
        return;

    // touche par mario
    if(knocked)
    {
        if(count < 10)
            setY(y()-1);
        else if(count < 20)
            setY(y()+1);

        count++;

        if(count >= 20)
        {
            knocked = false;
            count = 0;
        }
    }
    else if(destroyed)
    {
        setVisible(false);

        // Si la brick est détruite on spaw les débris de celle
        new BrickDebris(QPoint(x(),                  y()-15), LEFT);
        new BrickDebris(QPoint(x()+pixmap().width(), y()-15), RIGHT);
        new BrickDebris(QPoint(x()-5,                  y()-5), LEFT);
        new BrickDebris(QPoint(x()+pixmap().width()+5, y()-5), RIGHT);
    }
}

void Brick::hit(Item *what, Direction fromDir)
{
    // Si la brick est touchée par Mario d'en dessous
    Mario* mario = dynamic_cast<Mario*>(what);
    if(mario && fromDir == DOWN)
    {
        //Si mario est big on peut détruire les brick
        if(mario->isBig())
        {
            QSound::play(":/sons/block-break.wav");
            destroyed = true;
        }
        else
        {
            QSound::play(":/sons/block-hit.wav");
            knocked = true;
            count = 0;
        }
    }

}
