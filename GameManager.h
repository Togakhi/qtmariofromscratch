#pragma once
#ifndef LEVELMANAGER_H
#define LEVELMANAGER_H
#include <QGraphicsScene>
#include <string>
#include "Mario.h"
#include "Flag.h"


class GameManager
{
    private:
        Flag * flag;


    public:

        //charge les items du level passé en param et retourne l'objet mario
        static Mario * load(std::string level_name, QGraphicsScene * scene);


};

#endif // LEVELMANAGER_H
