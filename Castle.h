#ifndef CASTLE_H
#define CASTLE_H

#include "Inert.h"

class Castle : public Inert
{
    public:

        Castle(QPoint position);

        virtual void hit(Item *what, Direction fromDir);
};

#endif // CASTLE_H
