#ifndef PIPE_H
#define PIPE_H

#include "Inert.h"

class Pipe : public Inert
{
public:

    Pipe(QPoint position, int size);

    virtual void hit(Item *what, Direction fromDir);
};

#endif // PIPE_H
