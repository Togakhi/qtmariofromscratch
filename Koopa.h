#ifndef KOOPA_H
#define KOOPA_H

#include "Enemy.h"

class Koopa : public Enemy{

    protected:

        //si la tortue est dans sa carapace
        bool inShell;

        //Pixmaps
        QPixmap pixmap_walk[2];
        QPixmap pixmap_turtle;
        QPixmap pixmap_dead;

        static const int walk_frame = 10;

    public:

        //Getters&Setters
        inline bool isInShell() const {return inShell;}

        inline void setInShell(bool in) { inShell = in;}

        Koopa(QPoint pos, Direction direction = LEFT);

        virtual void moving();

        virtual void hurt();

        virtual void hit(Item *what, Direction fromDir);

};

#endif // KOOPA_H
