#include "BrickDebris.h"

BrickDebris::BrickDebris(QPoint pos, Direction dir) : Entity()
{
    setPixmap(QPixmap(":/images/brick-debris.bmp"));
    setPos(pos);

    untouchable = true;
    move = true;
    setDir(dir);
    slow = true;
    setZValue(10);
    fall = false;
    fall_speed = rand()%15;
    startJump();
}

void BrickDebris::hit(Item *what, Direction fromDir)
{
    what=0;
    fromDir=UNKNOWN;
    die();
}
