#include "Mushroom.h"
#include "Mario.h"
#include <QSound>


Mushroom::Mushroom(QPoint position) : Entity()
{
    //On initialise la texture du champignon
    setPixmap(loadTexture(":/images/mushroom-red.png"));
    //On initialise la position
    setPos(position);

    // On initialise les propriétés du champignon
    collectable = true;
    collidable = false;
    move = true;

    //Apparition vers le haut
    dir = UP;
    fall = false;
    move_speed = 1;
    fall_speed = 1;
    move_speed_div = 6;
    slow = true;
    setZValue(2);
    spawned_position = position;
    death_duration = 0;


    //On play le son du champignon
    QSound::play(":/sons/mushroom-appear.wav");
}

//Méthode de mouvements du champignon
void Mushroom::moving()
{
    // on commence l'animation du champignon
    walk_count++;

    //Si le msuhroom sort complètement du box,
    //Il devient collidable pour qu'il puisse interagir avec
    //Les autres entités
    if(y() == spawned_position.y()-pixmap().height())
    {
        collidable = true;
        fall = true;
        dir = RIGHT;
        move_speed_div = 2;
    }
}

void Mushroom::hit(Item *what, Direction fromDir)
{
    // si le mushroom entre en collision avec Mario,
    //Mario le mange
    Mario* mario = dynamic_cast<Mario*>(what);
    if(mario)
    {
        mario->mushroomEat();
        die();
        return;
    }

    //Si le mushroom fais une collision avec un autre item que Mario,
    //Il change de direction
    if(fromDir == RIGHT || fromDir == LEFT)
        setDir(inverseDirection(dir));
}

