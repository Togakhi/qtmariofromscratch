#ifndef BRICKDEBRIS_H
#define BRICKDEBRIS_H
#include "Entity.h"

class BrickDebris : public Entity
{
    protected:

        QPoint pos;

    public:

        BrickDebris(QPoint position, Direction dir);

        inline virtual void moving() {walk_count++;};

        virtual void hit(Item *what, Direction fromDir);
};

#endif // BRICKDEBRIS_H
