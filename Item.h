#ifndef ITEM_H
#define ITEM_H

#include <QGraphicsPixmapItem>
#include "tools.h"

class Item : public QGraphicsPixmapItem
{
    protected:

        bool walkable;      //si on peut marcher sur l'item
        bool collidable;    //si l'item peut entrer en contact avec d'autres entitées

    public:

        Item();

        inline bool isWalkable() const {return walkable;  }

        inline bool isCollidable() const {return collidable;}

        inline void setCollidable(bool isCol){collidable = isCol;}

        //fait bouger les items
        virtual void moving()=0;

        //vérifie l'état des items
        virtual void checkStatus()=0;

        virtual void hit(Item *what, Direction fromDir) = 0;

        //détecte les collisions
        virtual Direction collisionDir(Item * touchingItem);


};

#endif // ITEM_H
