#include "Mario.h"
#include "Entity.h"
#include "Enemy.h"
#include "Flag.h"
#include "tools.h"
#include "Castle.h"
#include <QSound>
#include <QDebug>
#include "Game.h"


Mario::Mario(QPoint position) : Entity()
{
    //Initialisation des états
    move = false;
    running = false;
    bouncing = false;
    big = false;
    transforming = false;
    gameFinished = false;
    transformation_counter = 0;
    scores = 0;

    //Initialisation des durées
    //La durée de saut est par défaut celle du saut du petit mario
    jump_duration =  jump_duration_small_mario;
    death_duration = 300;
    transformation_duration = freeze_duration;

    //Initialisation des différentes formes de mario
    texture_walk[0][0] = QPixmap(loadTexture(":/images/mario-small-walk-0.png"));
    texture_walk[0][1] = QPixmap(loadTexture(":/images/mario-small-walk-1.png"));
    texture_walk[0][2] = QPixmap(loadTexture(":/images/mario-small-walk-2.png"));
    texture_walk[1][0] = QPixmap(loadTexture(":/images/mario-big-walk-0.png"));
    texture_walk[1][1] = QPixmap(loadTexture(":/images/mario-big-walk-1.png"));
    texture_walk[1][2] = QPixmap(loadTexture(":/images/mario-big-walk-2.png"));
    texture_stand[0]   = QPixmap(loadTexture(":/images/mario-small-stand.png"));
    texture_stand[1]   = QPixmap(loadTexture(":/images/mario-big-stand.png"));
    texture_jump[0]    = QPixmap(loadTexture(":/images/mario-small-jump.png"));
    texture_jump[1]    = QPixmap(loadTexture(":/images/mario-big-jump.png"));
    texture_dead	   = QPixmap(loadTexture(":/images/mario-small-fall.png"));
    texture_small_to_big[0] = texture_stand[0];
    texture_small_to_big[1] = QPixmap(loadTexture(":/images/mario-med-stand.bmp"));
    texture_small_to_big[2] = texture_stand[1];
    texture_small_to_big[3] = QPixmap(loadTexture(":/images/mario-med-stand.bmp"));

    setPixmap(texture_stand[0]);
    setPos(position-QPoint(0, pixmap().height()));

    qDebug() << "mario created";
}

//Incrémenter le score après la collection d'un coin
void Mario::incrementScore()
{
    scores +=10;
}


// Méthode de saut de Mario
void Mario::toJump() {
    // Sortir de la méthode si Mario est
    //déjà en train de sauter
    if(jump)
        return;

    // Appel de la fonction de saut dans Entity
    startJump();

    // Si mario saute, on joue la music de saut
   if(jump)
       QSound::play(big ? ":/sons/jump-big.wav" : ":/sons/jump-small.wav");
}


// Méthode permettant à Mario de manger un champignon
// et de devenir grand
void Mario::mushroomEat()
{
    // Si Mario n'est pas déjà grand, on le fait grandir
    if(!big)
    {
        big = true;

        //On modifie la durée du saut correspondant au saut
        // de Mario quand il est grand
        jump_duration = jump_duration_big_mario;

        //On transforme Mario!
        startTransformation();

        //On joue le son de mario qui grandit
        QSound::play(":/sons/mushroom-eat.wav");

    }

}


//Méthode de transformation de mario
void Mario::startTransformation()
{
    //On met l'attribut de transformation à vrai
    transforming = true;

    //On modifie le compteur de transformation pour l'animation soit correctement gérée dans animate()
    transformation_counter = big ? 0 : 2*transf_div;
}


//Méthode qui met fin à la transformation
void Mario::endTransformation()
{
    //On set le booléen à faux
    transforming = false;

    //On réinitialise le compteur de transformation à 0
    transformation_counter = 0;

    //On fait une petite modif sur la hauteur (l'axe des y) pour
    //que Mario reste au même niveau du bas
    int dy = pixmap().height()-(big ? texture_stand[1] : texture_stand[0]).height();
    setY( y() + dy );
}

// Mario est blessé
void Mario::hurt()
{
    //Si mario est grand
    if(big)
    {
        //il devient petit
        big = false;

        jump_duration = jump_duration_small_mario;

        QSound::play(":/sons/shrink.wav");

        //Il devient intouchable pendant la transformation
        startUntouchable();

        startTransformation();

    }
    //S'il est petit il meurt
    else
        die();

}

void Mario::moving()
{
    //On gère le mode intouchable de mario
    if(untouchable)
        untouchable_count++;
        if(untouchable_count>untouchable_duration)
            endUntouchable();

    // On gère l'interaction de Mario avec les ennemis
    if(jump == false && bouncing)
    {
        bouncing = false;
        jump_duration = big ? jump_duration_big_mario : jump_duration_small_mario;
    }

    // On gère la transformation de Mario
    if(transforming)
    {
        transformation_counter++;
        if(transformation_counter > transformation_duration)
            endTransformation();
    }

    //On met à jour l'animation si la transformation est active
    if(transforming)
    {
        int prev = ((transformation_counter-1)/transf_div)%4;

        int curr = (transformation_counter/transf_div)%4;

        int dy = texture_small_to_big[prev].height()-texture_small_to_big[curr].height();

        setPixmap(texture_small_to_big[(transformation_counter/transf_div)%4]);

        setY(y()+dy);
    }
    //On set l'image s'il meurt ou en train de mourir
    else if(dying || dead)
        setPixmap(texture_dead);

    //On set l'image pour l'animation de marche
    else if(move && !jump && !fall)
        setPixmap(texture_walk[big][(walk_count++/(running ? running_div : walk_div))%3]);

    //On set l'image pour la chute
    else if(move || fall)
        setPixmap(texture_jump[big]);

    //On set l'image pour aucun mouvement
    else
        setPixmap(texture_stand[big]);

    //Change le sens de déplacement à gauche
    if(dir == LEFT)
        setPixmap(pixmap().transformed(QTransform().scale(-1,1)));

}


//Méthode simulant une interaction avec un ennemi ou le drapeau
void Mario::hit(Item *what, Direction fromDir)
{
    //Si Mario est touché par un ennemi
    Enemy *enemy = dynamic_cast<Enemy*>(what);
    Flag *flag = dynamic_cast<Flag*>(what);
    Castle *castle = dynamic_cast<Castle*>(what);

    if(enemy)
    {
        // Si la collision se fait du bas on tue l'ennemi
        if(fromDir == DOWN)
        {
            enemy->hurt();
            //Puis Mario bounce
            bounce();
        }
        // Dans les autres cas de collision, c'est Mario qui est mort/blessé
        else
           hurt();
    }
    else if(flag){

        if(fromDir == RIGHT)
        {
            //On enlève la collision du drapeau, pour que Mario puisse avancer
            flag->setCollidable(false);

            //On change la couleur du drapeau
            flag->setPixmap(QPixmap(loadTexture(":/images/flag-green.png")));

            flagTouched = true;
        }

        //Si le flag est touchée
    }else if(castle && flagTouched){

        //et le castle est touchée
        if(fromDir == RIGHT)
        {
            //On enlève la collision du drapeau, pour que Mario puisse avancer
            castle->setCollidable(false);

            //mario finis la partie en touchant le chateau
            if(pos().x()+15 == castle->pos().x())
                gameFinished = true;

        }
    }
}


// Méthode simulant un saut sur un ennemi
void Mario::bounce()
{
    //On met le booléan de tomber est à faux
    fall = false;

    //On met le saut à vrai
    bouncing = true;

    //On modifie le temps de saut
    jump_duration = jump_duration_tiny_mario;

    //Et on saute dès qu'on touche l'ennemi
    startJump();
}


//Surcharge de la méthode simulant la mort de l'entité (ici Mario)
void Mario::die()
{
    Entity::die();

    //On enlève la collision pour que Mario tombe du sol
    collidable = false;

    // musique liée à la mort de Mario
    QSound::play(":/sons/death.wav");
    Game::instance()->stopMusic();

    // freeze Mario avant qu'il tombe
    freezed();

    // faire un petit saut avant de tomber
    jump_speed = 1;
    fall_speed = 1;
    startJump();
}


void Mario::setRunning(bool isRunning) {

    //Ne rien faire si l'état est unchangé
    if(running == isRunning)
        return;

    //On affecte le nouvel état
    running = isRunning;

}
