#ifndef GROUND_H
#define GROUND_H

#include "Inert.h"
#include "Item.h"

class Ground : public Inert
{
    public:

        Ground(QRect rect);

        virtual std::string name() { return "Groud"; }
        virtual void hit(Item *what, Direction fromDir);
};

#endif // GROUND_H
