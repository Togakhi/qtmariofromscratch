#include "GameManager.h"
#include "Goomba.h"
#include "Ground.h"
#include "Box.h"
#include "Brick.h"
#include "Pipe.h"
#include "Goomba.h"
#include "Flag.h"
#include "Mushroom.h"
#include "Background.h"
#include "Koopa.h"
#include "Castle.h"

//Méthode d'initialisation des élements du jeu
Mario * GameManager::load(std::string level_name, QGraphicsScene *scene)
{
    Mario * mario = 0;


    if(level_name == "World-1-1"){
       //on crée le level et on ajoute les items

       //On init le background color
       scene->setBackgroundBrush(QBrush(QColor(99,133,251)));

       //On init le niveau du terrain
       int ground_level = 200;

       new Ground(QRect(0, ground_level, 69*16, 30));
       new Ground(QRect(71*16, ground_level, 69*16, 30));

       // Message Welcome
       new Background(QPoint(40,ground_level-5.5*16), ":/images/welcome.png");

       // On set les nuages/hills/bush
       new Background(QPoint(0,ground_level),       ":/images/hill-big.png");
       new Background(QPoint(11.5*16,ground_level), ":/images/bush-big.png");
       new Background(QPoint(16*16,ground_level),   ":/images/hill-small.png");
       new Background(QPoint(19.5*16,ground_level-9.5*16),  ":/images/cloud-small.png");
       new Background(QPoint(23.5*16,ground_level), ":/images/bush-small.png");
       new Background(QPoint(27.5*16,ground_level-8.5*16),  ":/images/cloud-big.png");
       new Background(QPoint(36.5*16,ground_level-9.5*16),  ":/images/cloud-med.png");
       new Background(QPoint(41.5*16,ground_level), ":/images/bush-med.png");
       new Background(QPoint(48*16,ground_level),   ":/images/hill-big.png");
       new Background(QPoint(59.5*16,ground_level), ":/images/bush-big.png");
       new Background(QPoint(64*16,ground_level),   ":/images/hill-small.png");



       //Les box et les bricks
       new Box(QPoint(16*16,ground_level-4*16));
       new Brick(QPoint(20*16,ground_level-4*16));
       new Box(QPoint(21*16,ground_level-4*16),VISIBLE,REDMUSH);
       new Brick(QPoint(22*16,ground_level-4*16));
       new Box(QPoint(23*16,ground_level-4*16));
       new Brick(QPoint(24*16,ground_level-4*16));
       new Box(QPoint(22*16,ground_level-8*16));
       new Box(QPoint(65*16,ground_level-5*16));

       // pipes
       new Pipe(QPoint(28*16, ground_level), 0);
       new Pipe(QPoint(38*16, ground_level), 1);
       new Pipe(QPoint(46*16, ground_level), 2);
       new Pipe(QPoint(57*16, ground_level), 2);

       //Koopa
       new Koopa(QPoint(25*16, ground_level));
       new Koopa(QPoint(30*16, ground_level));
       new Koopa(QPoint(40*16, ground_level));

       // Goombas
       new Goomba(QPoint(51*16, ground_level));
       new Goomba(QPoint(52.5*16, ground_level));


       //Deuxième partie du ground
       new Background(QPoint(71,ground_level),       ":/images/hill-big.png");
       new Background(QPoint(82.5*16,ground_level), ":/images/bush-big.png");
       new Background(QPoint(87*16,ground_level),   ":/images/hill-small.png");
       new Background(QPoint(90.5*16,ground_level-9.5*16),  ":/images/cloud-small.png");
       new Background(QPoint(94.5*16,ground_level), ":/images/bush-small.png");
       new Background(QPoint(98.5*16,ground_level-8.5*16),  ":/images/cloud-big.png");
       new Background(QPoint(107.5*16,ground_level-9.5*16),  ":/images/cloud-med.png");
       new Background(QPoint(112.5*16,ground_level), ":/images/bush-med.png");
       new Background(QPoint(118*16,ground_level),   ":/images/hill-big.png");


       //Les box et les bricks
       new Box(QPoint(74*16,ground_level-4*16));
       new Brick(QPoint(78*16,ground_level-4*16));
       new Box(QPoint(79*16,ground_level-4*16));
       new Brick(QPoint(80*16,ground_level-4*16));
       new Box(QPoint(81*16,ground_level-4*16));
       new Brick(QPoint(82*16,ground_level-4*16));
       new Brick(QPoint(83*16,ground_level-4*16));
       new Box(QPoint(22*16,ground_level-8*16));
       new Box(QPoint(82*16,ground_level-8*16),VISIBLE,REDMUSH);
       new Box(QPoint(83*16,ground_level-8*16));


       // pipes
       new Pipe(QPoint(92*16, ground_level), 0);
       new Pipe(QPoint(102*16, ground_level), 2);
       new Pipe(QPoint(112*16, ground_level), 1);

       //Koopa
       new Koopa(QPoint(98*16, ground_level));

       // Goombas
       new Goomba(QPoint(120*16, ground_level));

       //Flag
       new Flag(QPoint(129.5*16, ground_level));

       //Castle
       new Castle(QPoint(133.5*16, ground_level));


       //Mario
       mario = new Mario(QPoint(16*2.5, ground_level));


    }

    return mario;
}
