#include "Coin.h"
#include <QSound>

Coin::Coin(QPoint positionCoin) : Entity()
{
    position = positionCoin;

    // plusieurs image pour l'animation du coin
    texture_cycle[0] = loadTexture(":/images/coin-0.png");
    texture_cycle[1] = loadTexture(":/images/coin-1.png");
    texture_cycle[2] = loadTexture(":/images/coin-2.png");
    texture_cycle[3] = loadTexture(":/images/coin-3.png");

    // set la texture avec laquelle on commence et la position
    setPixmap(texture_cycle[0]);
    setPos(position);

    // set propertiés
    collectable = true;
    move = false;
    jump_speed = 1;
    fall = false;
    fall_speed =1 ;


    death_duration = 0;
    setZValue(10);

    // sauter
    startJump();

    // le sound du coin
    QSound::play(":/sons/coin.wav");
}

void Coin::moving()
{
    // si le coin tombe on le supprime de la partie
    if(fall && y() >= position.y())
        die();
    // sinon on continue son animation
    else
       setPixmap(texture_cycle[(walk_count++/anim_div)%3]);
}

void Coin::hit(Item *what, Direction fromDir)
{
    //Le coin n'a pas d'interraction avec les autres entités
    what=0;
    fromDir=UNKNOWN;
}
