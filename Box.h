#ifndef BOX_H
#define BOX_H

#include "Inert.h"

// Les objets qu'on peut retrouver dans les box
enum spawn_t {

    COIN,			// coin
    REDMUSH,		// Champignon rouge
};

enum visible_t {

    VISIBLE,		// entièrement visible
    HIDDEN,         // Caché par un brick
    INVISIBLE		// invisible
};


class Box:public Inert
{
    public:

        Box(QPoint position, visible_t _visible = VISIBLE, spawn_t _spawn = COIN);

        // getter
        visible_t getVisibility(){return visible;}

        virtual void moving() ;

        virtual void checkStatus();

        virtual void hit(Item *what, Direction fromDir);


    protected:

        QPixmap animation[3];       // animation
        QPixmap sprite_used;        // images après mario avoir toucher
        QPixmap sprite_brick;       // images pour le brick


        bool used;                  // si le box est utlisé
        bool knocked;               // si le box est touché par mario


        visible_t visible;
        spawn_t spawn;

        static const int knock_frame = 40;

        //compteur du nombre d'images depuis la dernière fois où Mario a été frappé
        int  count;

};

#endif // BOX_H
