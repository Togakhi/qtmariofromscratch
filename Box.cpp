#include "Box.h"
#include "Mario.h"
#include "Coin.h"
#include "Mushroom.h"
#include <QSound>


Box::Box(QPoint position, visible_t _visibility, spawn_t _spawnable) : Inert()
{
    // Plusieurs image pour l'animation du box
    animation[0] = QPixmap(":/images/box-0.bmp");
    animation[1] = QPixmap(":/images/box-1.bmp");
    animation[2] = QPixmap(":/images/box-2.bmp");
    sprite_used     = QPixmap(":/images/box-used.bmp");
    sprite_brick	 = QPixmap(":/images/brick.bmp");

    // set propertiés
    used = false;
    knocked = false;
    count = 0;
    visible = _visibility;
    spawn = _spawnable;
    if(visible == INVISIBLE)
        this->setOpacity(0.0);

    // set texture et position
    setPixmap(animation[0]);

    setPos(position);

    setZValue(3);
}

void Box::checkStatus()
{
    if(used)
        setPixmap(sprite_used);
    else if(visible == HIDDEN)
        setPixmap(sprite_brick);
    else
        setPixmap(animation[(count++/knock_frame)%3]);
}

void Box::hit(Item *what, Direction fromDir)
{

    // Mario touche les box en sautant
    Mario* mario = dynamic_cast<Mario*>(what);

    if(mario && fromDir == DOWN)
    {
        if(used)
            QSound::play(":/sons/block-hit.wav");
        else
        {
            // faire apparaître l'objet coin
            if(spawn == COIN){
                new Coin(QPoint(x(), y()-20));
                mario->incrementScore();
            }
            else if(spawn == REDMUSH)
            {
                //On génère un nouveau mushroom rouge
                new Mushroom(QPoint(x(), y()));
            }

            // set flags / counters
            knocked = true;
            used = true;
            count = 0;
        }
    }
}

void Box::moving()
{
    if(knocked)
    {
        visible = VISIBLE;
        setOpacity(1.0);

        if(count < 10)
            setY(y()-1);
        else if(count < 20)
            setY(y()+1);

        if(count > 20)
            knocked = false;

        count++;
    }
}


