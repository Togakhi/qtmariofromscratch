#ifndef MARIO_H
#define MARIO_H

#include "Entity.h"

#include <QPoint>
//Classe représentant le personnage Mario
class Mario : public Entity
{

    protected:

    //Paramètres physiques du personnage
    //Temps de saut de mario étant petit
    static const int jump_duration_tiny_mario  = 30;

    //Temps de saut de mario étant normal
    static const int jump_duration_small_mario = 70;

    //Temps de saut de mario étant grand
    static const int jump_duration_big_mario   = 90;

    //Diviseurs d'animation
    //(inversement proportionnels à la vitesse d'animation)
    //Animation de marche
    static const int walk_div    = 6;

    //Animation de course
    static const int running_div = 4;

    //Animation de transformation
    static const int transf_div  = 5;

    // Animations
    //Grande et standard animation de marche
    QPixmap texture_walk[2][3];

    //Grande et standard animation de stand
    QPixmap texture_stand[2];

    //Grande et standard animation de saut
    QPixmap texture_jump[2];

    //Animation de mort de mario
    QPixmap texture_dead;

    //Animation de transformation de mario (petit=>grand)
    QPixmap texture_small_to_big[4];

    //Variables de description de l'état de mario
    //Booléen précisant si mario est grand
    bool big;

    //Si mario bondit sur un ennemi
    bool bouncing;

    //Si mario est en train d'avancer
    bool running;

    //Si mario change de taille
    bool transforming;

    //Si mario à touché le drapeau
    bool flagTouched;

    //Si mario rentre dans le chateau (à voir)
    bool gameFinished;

    // compteurs
    int transformation_counter;

    // scores
    int scores;

    //limite des compteurs
    int transformation_duration;

    //Méthodes d'actions

    // bounce on enemies
    void bounce();

    // début/fin de transformation
    void startTransformation();
    void endTransformation();



public:
    Mario(QPoint position);

    //Accesseurs
    bool isBig() {
        return big;
    }

    inline bool isTransforming() {return transforming;}

    inline bool isGameFinished() const {return gameFinished;}

    inline int getScores() const {return  scores;}

    void setRunning(bool isRunning);

    // Mario prend des dégats
    void hurt();

    // Mario saute
    void toJump();

    // Gagne les points losque mario trouve les coins
    void incrementScore();

    // Mario mange un champignon et devient grand
    void mushroomEat();

    inline void setFalling(bool isFalling){
        fall = isFalling;
    }

    //Méthodes virtuelles
    virtual void moving();

    virtual void hit(Item *what, Direction fromDir);

    //Méthode simulant la mort de mario
    virtual void die();

};



#endif // MARIO_H
