#ifndef COIN_H
#define COIN_H

#include "Entity.h"

class Coin : public Entity
{

    protected:

        //La position à laquelle le coin apparaît
        QPoint position;

        //L'animation du coin en tournant (4 étapes)
        QPixmap texture_cycle[4];

        // animation divisors (inversely proportional to animation speed)
        static const int anim_div = 6;	// main animation

    public:

        Coin(QPoint position);

        //Animation du coin lorsqu'il sort du block
        virtual void moving();

        //Mario gagne le coin
        virtual void hit(Item *what, Direction fromDir);

};

#endif // COIN_H
