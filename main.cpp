#include "Game.h"
#include "Brick.h"
#include "Box.h"
#include "BrickDebris.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Game::instance()->show();
    return a.exec();
}
