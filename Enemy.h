#ifndef ENEMY_H
#define ENEMY_H

#include "Entity.h"


//Entités qui peuvent blesser mario et que mario peut blesser
class Enemy : public Entity
{

    public:

        Enemy();

        virtual void moving() = 0;

        virtual void hurt() = 0;

        virtual void hit(Item *what, Direction fromDir) = 0;

};

#endif // ENEMY_H
