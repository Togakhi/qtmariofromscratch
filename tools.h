#ifndef TOOLS_H
#define TOOLS_H

#include <QPoint>
#include <QPixmap>
#include <QBitmap>
#include <QColor>
#include <string>

enum Direction {UNKNOWN, RIGHT,LEFT, DOWN, UP};


//Inverse la direction
static Direction inverseDirection (Direction dir){
    if(dir == RIGHT)
        return LEFT;
    else if(dir == LEFT)
        return RIGHT;
    else if(dir == UP)
        return DOWN;
    else if(dir == DOWN)
        return UP;
    else
        return UNKNOWN;
}


//charge l'animation à partir d'un fichier
static QPixmap loadTexture(const std::string file, QColor mask_color = Qt::magenta)
{
    QPixmap pixmap(file.c_str());
    pixmap.setMask(pixmap.createMaskFromColor(mask_color));
    return pixmap;
}


#endif // TOOLS_H
